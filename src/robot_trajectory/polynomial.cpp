#include <umrob/polynomial.h>
#include <math.h>
#include <iostream>

namespace umrob {

Polynomial::Constraints& Polynomial::constraints() {
    return constraints_;
}

const Polynomial::Constraints& Polynomial::constraints() const {
    return constraints_;
}



void Polynomial::computeCoefficients() {

double dx=constraints_.xf-constraints_.xi;
    constraints_.xi=0;
coefficients_.a= -(12*constraints_.yi - 12*constraints_.yf+6*dx*constraints_.dyf+6*dx*constraints_.dyi-constraints_.d2yf*dx*dx+constraints_.d2yi*dx*dx)/(2*pow(dx,5));
coefficients_.b= (30*constraints_.yi - 30*constraints_.yf+14*dx*constraints_.dyf+16*dx*constraints_.dyi-2*constraints_.d2yf*dx*dx+3*constraints_.d2yi*dx*dx)/(2*pow(dx,4));
coefficients_.c= -(20*constraints_.yi - 20*constraints_.yf+8*dx*constraints_.dyf+12*dx*constraints_.dyi-constraints_.d2yf*dx*dx+3*constraints_.d2yi*dx*dx)/(2*pow(dx,3));
coefficients_.d=constraints_.d2yi/2;
coefficients_.e=constraints_.dyi;
coefficients_.f=constraints_.yi;
}


//! y = ax^5 + bx^4 + cx^3 + dx^2 + ex + f
double Polynomial::evaluate(double x) {

double y;
y=coefficients_.a*pow(x,5)+coefficients_.b*pow(x,4)+coefficients_.c*pow(x,3)+coefficients_.d*pow(x,2)+coefficients_.e*x+coefficients_.f;
if (constraints_.xf> constraints_.xi)
   {if(x>constraints_.xf)
       y=constraints_.yf;  
    if(x<constraints_.xi)
       y=constraints_.yi;}
else
   {if (constraints_.xf< constraints_.xi)
      {if(x<constraints_.xf)
          y=constraints_.yf;  
       if(x>constraints_.xi)
          y=constraints_.yi;}
    else
std::cout<<"xi=xf";
 
    } 
return y;
}

//! dy = 5ax^4 + 4bx^3 + 3cx^2 + 2dx + e
double Polynomial::evaluateFirstDerivative(double x) {

double dy;
 dy=5*coefficients_.a*pow(x,4)+4*coefficients_.b*pow(x,3)+3*coefficients_.c*pow(x,2)+2*coefficients_.d*x+coefficients_.e;
 if (constraints_.xf> constraints_.xi)
    {if(x>constraints_.xf)
       dy=constraints_.dyf;  

     if(x<constraints_.xi)
       dy=constraints_.dyi;
    }
else
{if (constraints_.xf< constraints_.xi)
    {if(x<constraints_.xf)
       dy=constraints_.dyf;  
     if(x>constraints_.xi)
       dy=constraints_.dyi;
    }
 else
 std::cout<<"xi=xf";
}
return dy;
}

//! d2y = 20ax^3 + 12bx^2 + 6cx + 2d
double Polynomial::evaluateSecondDerivative(double x) {
    double d2y;
 d2y=20*coefficients_.a*pow(x,3)+12*coefficients_.b*pow(x,2)+12*coefficients_.c*x+2*coefficients_.d;
 if (constraints_.xf> constraints_.xi)
    {if(x>constraints_.xf)
       d2y=constraints_.d2yf;  
     if(x<constraints_.xi)
       d2y=constraints_.d2yi;
    }
else
    {if (constraints_.xf< constraints_.xi)
       {if(x<constraints_.xf)
           d2y=constraints_.d2yf;  
        if(x>constraints_.xi)
           d2y=constraints_.d2yi;
        }
     else 
     std::cout<<"xi=xf";
}
    return d2y;
}


}