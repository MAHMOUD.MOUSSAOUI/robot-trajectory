import math
import numpy as np


class Membres:
    global d5
    d5=-0.15
    global PI
    PI = math.pi
    global DH
    DH=np.mat([[0,0,0,0],[(PI/2),0,0,0.185],[(PI/2),0,0,-0.148],[(-PI/2),(-0.01),0,0],[(-PI/2),0,0,0.215]])
    
    def __init__(self, group, dh, robot):
        self.group = group
        self.dh = dh
        self.robot = robot

    def matrix_transfo(DH):
        global T01,T02,T03,T04,T05,Ti
        for i in range (5):
            Ti[0,0]=math.cos(DH[i,2])
            Ti[0,1]=-math.sin(DH[i,2])
            Ti[0,2]=0.0
            Ti[0,3]=DH[i,1]
            Ti[1,0]=math.cos(DH[i,0])*math.sin(DH[i,2])
            Ti[1,1]=math.cos(DH[i,0])*math.cos(DH[i,2])
            Ti[1,2]=-math.sin(DH[i,0])
            Ti[1,3]=-DH[i,3]*math.sin(DH[i,0])
            Ti[2,0]=math.sin(DH[i,0])*math.sin(DH[i,2])
            Ti[2,1]=math.sin(DH[i,0])*math.cos(DH[i,2])
            Ti[2,2]=math.cos(DH[i,0])
            Ti[2,3]=DH[i,3]*math.cos(DH[i,0])
            Ti[3,0]=0
            Ti[3,1]=0
            Ti[3,2]=0
            Ti[3,3]=1
            
            if i==0:
                T01=Ti
            if i==1:
                T02=np.dot(Ti,T01)
            if i==2:
                T03=np.dot(Ti,T02)
            if i==3:
                T04=np.dot(Ti,T03)
            if i==4:
                T05=np.dot(Ti,T04)
                
        return T05

    def calc_mgd(theta):
        O56=np.mat([[d5],[0],[0],[1]])
        for i in range(5):
            DH[i,2]=Membres.theta[i]
        mgd=np.dot(Membres.matrix_transfo(Membres.DH),O56)
        mgd=mgd[0:3]  
        return mgd

    def jacobienne(theta):
        global mat_jacob_inv
        Z01=T01[0:3,2]
        Z02=T02[0:3,2]
        Z03=T03[0:3,2]
        Z04=T04[0:3,2]
        Z05=T05[0:3,2]  
        P01=T01[0:3,3]
        P02=T02[0:3,3]
        P03=T03[0:3,3]
        P04=T04[0:3,3]
        P05=T05[0:3,3]
        
        P15=P05-P01
        P25=P05-P02
        P35=P05-P03
        P45=P05-P04
        
        a=np.cross(Z01,P15)
        b=np.cross(Z02,P25)
        c=np.cross(Z03,P35)
        d=np.cross(Z01,P45)
        
        J05=np.zeros((6,5))
        J05[0:3,0]=a.T
        J05[0:3,1]=b.T
        J05[0:3,2]=c.T
        J05[0:3,3]=d.T
        J05[0:3,4]=np.zeros((1,3))
        J05[3:6,0]=Z01.T
        J05[3:6,1]=Z02.T
        J05[3:6,2]=Z03.T
        J05[3:6,3]=Z04.T
        J05[3:6,4]=Z05.T
        
        D=np.mat([[0,-d5*T05[2,0],d5*T05[1,0]],
                  [d5*T05[2,0],0,-d5*T05[0,0]],
                  [-d5*T05[1,0],d5*T05[0,0],0]])
        
        G=np.zeros((6,6))
        G[0:3,0:3]=np.eye(3)
        G[3:6,3:6]=np.eye(3)
        G[0,3,3:6]=D
        mat_jacob=np.dot(G,J05)
        mat_jacob_inv=np.pinv(mat_jacob)
        return mat_jacob 

    def gen_traj(q1f,q2f,q3f,q4f,q5f,tf):
            global a10,a20,a30,a40,a50,a11,a12,a13,a21,a22,a23,a31,a32,a33,a41,a42,a43,a51,a52,a53
            a10=DH[0,2]
            a20=DH[1,2]
            a30=DH[2,2]
            a40=DH[3,2]
            a50=DH[4,2]
            a11=0
            a12=(3/pow(tf,2))*(q1f-a10)
            a13=(-2/pow(tf,3))*(q1f-a10)
            a21=0
            a22=(3/pow(tf,2))*(q2f-a20)
            a23=(-2/pow(tf,3))*(q2f-a20)
            a31=0
            a32=(3/pow(tf,2))*(q3f-a30)
            a33=(-2/pow(tf,3))*(q3f-a30)
            a41=0
            a42=(3/pow(tf,2))*(q4f-a40)
            a43=(-2/pow(tf,3))*(q4f-a40)
            a51=0
            a52=(3/pow(tf,2))*(q5f-a50)
            a53=(-2/pow(tf,3))*(q5f-a50)
           
            DH[0,2]=q1f 
            DH[1,2]=q2f
            DH[2,2]=q3f
            DH[3,2]=q4f
            DH[4,2]=q5f
            #génération de la trajectoire 
            #return résultat
  

    def eval_traj():
            tf=2
            Membres.gen_traj(PI/2,(-PI)/2,0,PI/2,0)
            count=0
            while count<=tf:
                q1=a10+a11*count+a12*pow(count,2)+a13*pow(count,3)
                q2=a20+a21*count+a22*pow(count,2)+a23*pow(count,3)
                q3=a30+a31*count+a32*pow(count,2)+a33*pow(count,3)
                q4=a40+a41*count+a42*pow(count,2)+a43*pow(count,3)
                q5=a50+a51*count+a52*pow(count,2)+a53*pow(count,3)
                theta=np.mat([[q1],[q2],[q3],[q4],[q5]])
                resultat1=Membres.calc_mgd(theta)
                
                q1p=a11+2*a12*count+3*a13*pow(count,2)
                q2p=a21+2*a22*count+3*a23*pow(count,2)
                q3p=a31+2*a32*count+3*a33*pow(count,2)
                q4p=a41+2*a42*count+3*a43*pow(count,2)
                q5p=a51+2*a52*count+3*a53*pow(count,2)
                thetap=np.mat([[q1p],[q2p],[q3p],[q4p],[q5p]])
                r=Membres.jacobienne(theta)*thetap
                resultat2=r[0:3]
                count=count+0.01
                resultat=np.mat([resultat1,resultat2])
                return resultat


poppy_head_config = {
    'controllers': {
        'my_dxl_controller': {
            'port': '/dev/ttyACM0',  # Depends on your OS
            'sync_read': False,
            'attached_motors': ['head'],  # You can mix motorgroups or individual motors
        },
    },

    'motorgroups': {
        'head': ['head_y'],
    },

    'motors': {
        'head_y': {
            'id': 1,
            'type': 'AX-12',
            'orientation': 'INdirect',
            'offset': 20.0,
            'angle_limit': (-45, 6),
        },
    },
}


